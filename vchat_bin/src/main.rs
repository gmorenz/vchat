#![feature(box_syntax, stmt_expr_attributes, conservative_impl_trait)]

extern crate rscam;
#[macro_use] extern crate rust_imgui as imgui;
extern crate time;
extern crate glutin;
extern crate gl;
extern crate clap;
extern crate vchat;

use clap::{App, Arg};

use vchat::{Sender, Reciever, Pipe};

use std::io::prelude::*;
use std::fs::File;
use std::net::{UdpSocket, SocketAddr};
use std::str::FromStr;
use std::ops::Deref;

mod gui;
use gui::Gui;

fn fmt_data_amount(amount: usize) -> String {
    if amount > 1_000_000_000_000 {
        format!("{:.3} Tb", amount as f64 / 1_000_000_000_000.)
    }
    else if amount > 1_000_000_000 {
        format!("{:.3} Gb", amount as f64 / 1_000_000_000.)
    }
    else if amount > 1_000_000 {
        format!("{:.3} Mb", amount as f64 / 1_000_000.)
    }
    else if amount > 1_000 {
        format!("{:.3} Kb", amount as f64 / 1_000.)
    }
    else {
        format!("{} bytes", amount)
    }
}

trait Camera {
    fn get(&mut self) -> Option<Box<Deref<Target = [u8]>>>;
}

impl Camera for rscam::Camera {
    fn get(&mut self) -> Option<Box<Deref<Target = [u8]>>> {
        self.capture().map(|x| (box x) as Box<Deref<Target = [u8]>>).ok()
    }
}

impl Camera for File {
    fn get(&mut self) -> Option<Box<Deref<Target = [u8]>>> {
        let mut buf = box [0; 640 * 480 * 2];
        if let Ok(()) = self.read_exact(&mut *buf) {
            Some(box (buf as Box<[u8]>))
        }
        else {
            println!("End of recording");
            None
        }
    }
}

fn camera() -> rscam::Camera {
    let mut camera = rscam::new("/dev/video0").expect("No camera at /dev/video0");

    camera.start(&rscam::Config {
        interval: (1, 30), // TODO: Lower framerate
        resolution: (640, 480),
        format: b"YUYV",
        field: rscam::FIELD_NONE, // TODO: Best Option
        nbuffers: 2,
    }).unwrap();

    camera
}

fn record(filename: &str) -> ! {

    let camera = camera();
    let mut outfile = File::create(filename).unwrap();
    loop {
        let frame = camera.capture().unwrap();
        outfile.write(&frame).unwrap();
    }
}

fn main() {
    let arguments = App::new("vchat debug binary")
        .arg( Arg::with_name("record").long("record").takes_value(true) )
        .arg( Arg::with_name("playback").long("playback").takes_value(true) )
        .arg( Arg::with_name("patches per frame").long("patches-per-frame").takes_value(true) )
        .arg( Arg::with_name("no render").long("no-render") )
        .arg( Arg::with_name("send port").long("send-port").default_value("12345") )
        .arg( Arg::with_name("recv port").long("recv-port").default_value("12346") )
        .arg( Arg::with_name("peer send").long("peer-send").default_value("127.0.0.1:12345"))
        .arg( Arg::with_name("peer recv").long("peer-recv").default_value("127.0.0.1:12346"))
        .get_matches();

    if let Some(filename) = arguments.value_of("record") {
        println!("Recording to: {}", filename);
        record(filename);
    }

    let mut camera = if let Some(filename) = arguments.value_of("playback") {
        Box::new(File::open(filename).unwrap()) as Box<Camera>
    }
    else {
        Box::new(camera()) as Box<Camera>
    };

    let mut patches_per_frame =
        if let Some(val) = arguments.value_of("patches per frame") {
            u32::from_str(val).expect("Need a number of patches per frame") as i32
        }
        else {
            120
        };

    let peer_send = arguments.value_of("peer send").unwrap();
    let peer_recv = arguments.value_of("peer recv").unwrap();
    let send_port = u16::from_str(arguments.value_of("send port").unwrap())
        .expect("Expected a numeric send port");
    let recv_port = u16::from_str(arguments.value_of("recv port").unwrap())
        .expect("Expected a numeric recv port");

    let render = ! arguments.is_present("no render");

    let mut gui = Gui::new();


    let mut reciever = {
        let socket = UdpSocket::bind(("127.0.0.1", recv_port)).unwrap();
        let addr = SocketAddr::from_str(peer_send).expect("Expected peer-send in the form 1.2.3.4:5678");
        let pipe = Pipe::new(socket, addr);
        Reciever::new(pipe)
    };

    let mut sender = {
        let socket = UdpSocket::bind(("127.0.0.1", send_port)).unwrap();
        let addr = SocketAddr::from_str(peer_recv).expect("Expected peer-recv in the form 1.2.3.4:5678");
        let pipe = Pipe::new(socket, addr);
        Sender::new(pipe)
    };

    let output_image = gui::ChannelImage::new(gui.glstate.yuv_program);
    let target_image = gui::ChannelImage::new(gui.glstate.yuv_program);
    let model_image = gui::ChannelImage::new(gui.glstate.yuv_program);

    // Hack to close window
    if !render {
        use std::mem;
        let dummy = unsafe { mem::uninitialized() };
        drop( mem::replace(&mut gui, dummy) );
    }

    let mut total_frames = 0;
    let mut total_send_data = 0;
    let mut total_send_time = 0;
    let mut total_recv_time = 0;
    'main_loop: while let Some(frame) = camera.get() {
        total_frames += 1;
        if render {
            gui.new_frame();
            // Poll for events first or you're going to have a bad time.
            for event in gui.window.poll_events() {
                // Hacky workaround because I don't want to make a gui toolkit right now.
                gui::imgui_check_event(&mut gui.glstate, &event);
                match event {
                    glutin::Event::Closed => break 'main_loop,
                    _ => ()
                }
            }

            imgui::begin(imstr!("Debug Info"), &mut true, imgui::ImGuiWindowFlags_None);
            imgui::text(imstr!("Average {:.3} ms/frame ({:.1} fps)", 1000.0 / imgui::get_io().framerate,
                imgui::get_io().framerate));
            imgui::separator();
            imgui::end();
        }

        {
            sender.model.update_local_yuyv480(&frame);

            let start_send = time::precise_time_ns();
            sender.reset_stats();
            {
                while sender.patches_sent < patches_per_frame && sender.run() {}
                // TODO: What is this for, how do I get rid of it?
                sender.con.send_now();
            }
            let end_send = time::precise_time_ns();

            total_send_time += end_send - start_send;
            total_send_data += sender.data_sent;

            // Render and Debug Text
            if render {
                imgui::begin(imstr!("Send Streams"), &mut true, imgui::ImGuiWindowFlags_None);
                target_image.render(sender.model.target_image.as_ptrs());
                model_image.render(sender.model.client_image.as_ptrs());
                imgui::end();

                imgui::begin(imstr!("Debug Info"), &mut true, imgui::ImGuiWindowFlags_None);
                imgui::text(imstr!("time spent on sending patches: {:.3}", (end_send - start_send) as f32 / 1_000_000.));
                imgui::text(imstr!("patches sent: {}", sender.patches_sent));
                let tmp = imstr!("{} patches per frame", patches_per_frame);
                // TODO: Checkbox?
                imgui::slider_int(imstr!(""), &mut patches_per_frame, 1, 3600, tmp);
                imgui::separator();
                if sender.packets_acked + sender.packets_lost != 0 {
                    imgui::text(imstr!("packets acked / packets lost: {} / {}   :   % {}",
                        sender.packets_acked, sender.packets_lost,
                        sender.packets_lost / (sender.packets_lost + sender.packets_acked)));
                    imgui::separator();
                }
                imgui::text(imstr!("send side, data sent: {}", fmt_data_amount(30 * sender.data_sent)));
                imgui::end();
            }
        }

        {
            reciever.reset_stats();
            let start_recv = time::precise_time_ns();
            while reciever.run() {}
            let end_recv = time::precise_time_ns();
            total_recv_time += end_recv - start_recv;


            // Render and Debug text
            if render {
                imgui::begin(imstr!("Recv Stream"), &mut true, imgui::ImGuiWindowFlags_None);
                output_image.render(reciever.model.image.as_ptrs());
                imgui::end();

                imgui::begin(imstr!("Debug Info"), &mut true, imgui::ImGuiWindowFlags_None);
                imgui::text(imstr!("recv side, data sent: {}", fmt_data_amount(30 * reciever.data_sent)));
                imgui::text(imstr!("time spend on recv patches: {}", (end_recv - start_recv) as f32 / 1_000_000.));
                imgui::end();
            }
        }

        if render { gui.render() };
    }

    if !render {
        ::std::mem::forget(gui);
    }

    println!("total sendside data sent: {}B", total_send_data);
    println!("total send time: {}ns", total_send_time);
    println!("total recv time: {}ns", total_recv_time);
    println!("total frames: {}", total_frames);
    println!("Average data: {}KB, send time: {}ms, recv time: {}ms",
        total_send_data as f64 / 1024.0 / total_frames as f64,
        total_send_time as f64 / 1_000_000.0 / total_frames as f64,
        total_recv_time as f64 / 1_000_000.0 / total_frames as f64
    );
}