# Vchat

This is a (very!) WIP video chat library inspired by [this](https://bengarney.com/2016/06/25/video-conference-part-1-these-things-suck/) series of blog posts. Currently implemented we have

 - A basic debugging gui.
 - A UDP networking model that updates the portions of the image most in need of being updated.
 - Facilities to record and replay videos, primarily for benchmarking purposes.
 - Arithmetic encoding compression, with various models, for the chunks being sent over

To use run the development gui run `cargo run --release` in the `vchat_bin` folder with a recent nightly version of rust (tested only on Linux, with `rustc 1.14.0-nightly (5665bdf3e 2016-11-02)`). I recommend installing and managing rust versions using [rustup](rustup.rs) to allow for this. Various options can be used via command line arguments, passed through cargo via `cargo run --release -- <arguments>`, e.g. `cargo run --release -- --help`. By default the gui streams video from a webcam to itself.

## High level architecture

A sender has a target image (which is periodically updated with a new image), and a model of the image it believes the reciever has. The image is split into 16x16 chunks. We then create diffs between the chunks in the target image and client image, and send those that decrease the difference between the images by the most.

## Network model

From the sending side we send packets that look like^1

`| packet id | highest acked packet id recieved | sequence of patches |`

After each packet arrives^2 we send a ack packet conaining the ids of each packet recieved since we sent the ack with id `highest acked packet id`. We declare lost and discard upon reciept any packets with ids lower (older) than the 5th most recent packet recieved (this is important because then the sender can discard the diffs generated for those packets instead of applying them or storing them forever).

Currently no UDP hole punching is implemented.

^1 I intend to eventually wrap this in authenticated encryption (probably AES OCB), which
would allow seamless reconnects when roaming as in mosh. If keys were exchanged securely this
would also bring substantial privacy and some security benefits.

^2 Often it would make sense to delay doing this until after recieving multiple packets,
this hasn't been implemented yet.

## Compression

Currently packets are compressed via [arithmetic encoding](https://en.wikipedia.org/wiki/Arithmetic_encoding) assuming approximately follow a cauchy distribution^3 centered at the previous number decoded (representing change in one YUV colour channel of a pixel). Significant work remains to be done in this area including experimenting with different schemes, looking at multiple geometrically related pixels instead of just the previous one, etc. Performance of arithmetic encoding has turned out to be a major concern and require significant optimization effort.

Also implemented, but not currently used, is a more traditional discrete cosine transform. Given that I'm interested in using arithmetic encoding versus more traditional run length encoding I'm not sure that it will actually be useful.

The arithmetic encoding has been implemented as a library (Arithmancy) that can be used either as a stand alone crate, or (to allow for it to be optimized with the code that runs it) as a module in another crate, as used in vchat.

^3 The cauchy distribution is discreteized by taking intevals in the CDF. The area under the unused tails is added back proportionally to the rest of the distribution.