pub struct BitIter<'a> {
    pub data: &'a[u8],
    pub index: usize
}

impl <'a> Iterator for BitIter<'a> {
    type Item = bool;
    fn next(&mut self) -> Option<bool> {
        if self.index < 8 * self.data.len() {
            let index = self.index;
            self.index += 1;
            Some(self.data[index / 8] & (1 << index % 8) != 0)
        }
        else {
            None
        }
    }
}