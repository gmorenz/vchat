#![feature(zero_one, box_syntax, stmt_expr_attributes, core_intrinsics)]

#![cfg_attr(feature = "trace_", plugin(trace))]
#![cfg_attr(feature = "trace_", allow(unused_unsafe))]
#![cfg_attr(feature = "trace_", feature(plugin, custom_attribute))]
#[cfg(feature = "trace_")]
pub static mut depth: u32 = 0;

extern crate byteorder;
extern crate time;

use std::collections::HashMap;

mod bitvec;
mod bititer;
mod image;
#[allow(dead_code)]
mod dct;

mod model;
use model::Patch;
#[cfg(feature = "send")]
use model::SendModel;
#[cfg(feature = "recv")]
use model::RecvModel;


mod net;
pub use net::Pipe;
#[cfg(feature = "send")]
use net::{SendConnection, SendEvent};
#[cfg(feature = "recv")]
use net::{RecvConnection, RecvEvent};

#[cfg(feature = "send")]
pub struct Sender {
    pub model: SendModel,
    pub con: SendConnection,
    packets_in_flight: HashMap<u32, Vec<u16>>, // packet_id, block_ids

    // Informational Statistics
    // TODO: Switch i32 to u32 (i32 for convenience with imgui right now)
    pub data_sent: usize,
    pub patches_sent: i32,
    pub packets_acked: i32,
    pub packets_lost: i32
}

impl Sender {
    pub fn new(pipe: Pipe) -> Sender {
        Sender {
            model: SendModel::new(),
            con: SendConnection::new(pipe),
            packets_in_flight: HashMap::new(),

            patches_sent: 0,
            packets_acked: 0,
            packets_lost: 0,
            data_sent: 0,
        }
    }

    pub fn run(&mut self) -> bool {
        match self.con.poll() {
            SendEvent::PacketArrived(x) => {
                self.packets_acked += 1;
                let patches = self.packets_in_flight.remove(&x).unwrap();
                for patch in patches {
                    self.model.apply_client_patch(patch);
                }
            },
            SendEvent::PacketLost(x) => {
                    self.packets_lost += 1;
                    let patches = self.packets_in_flight.remove(&x).unwrap();
                    for patch in patches {
                        self.model.forget_client_patch(patch);
                    }
            },
            // TODO: On both of the sends, make them 'schedule send' somehow.
            SendEvent::None => {
                if let Some(patch) = self.model.diff() {
                    let bytes = patch.to_bytes();
                    if bytes.len() <= self.con.space_in_packet() {
                        self.patches_sent += 1;
                        let packet_id = self.con.add_data(&bytes);
                        let patch_ids = self.packets_in_flight.entry(packet_id).or_insert(vec!());
                        patch_ids.push(patch.block_id);
                    }
                    else {
                        // Out of space in packet, might as well send it
                        self.data_sent += self.con.send_now();
                        // TODO: Decide whether to store patch (more efficient) or
                        // stick patch in next packet (will have better information for encoding).
                        // Doing the second has some implications for when I start limiting bandwidth
                        // so for now I'm doing the first.
                        self.model.forget_client_patch(patch.block_id);
                    }
                }
                else {
                    // Out of patches we can create, sending what we have right now because
                    //  a) The patches in the packet could be blocking more patches from being generated
                    //  b) Ignoring that special case, it *still* seems like probably the right thing to do.
                    self.data_sent += self.con.send_now();
                    // Return false since we're out of patches to ack and send
                    return false
                }
            }
        }

        true
    }

    pub fn reset_stats(&mut self) {
        self.patches_sent = 0;
        self.packets_acked = 0;
        self.packets_lost = 0;
        self.data_sent = 0;
    }
}

pub struct Reciever {
    pub model: RecvModel,
    con: RecvConnection,

    // Informational Statistics
    pub data_sent: usize,
}

impl Reciever {
    pub fn new(pipe: Pipe) -> Reciever {
        Reciever {
            model: RecvModel::new(),
            con: RecvConnection::new(pipe),

            data_sent: 0,
        }
    }

    pub fn run(&mut self) -> bool {
        match self.con.poll(Patch::from_bytes) {
            RecvEvent::RecievedPatch(patch) => {
                self.model.apply_patch(patch);
                true
            }
            RecvEvent::None => false,
        }
    }

    pub fn reset_stats(&mut self) {
        self.data_sent = 0;
    }
}