use std::net::{UdpSocket, SocketAddr};
use std::fmt;

use super::MAX_PACKET_SIZE;

pub struct Pipe {
    socket: UdpSocket,
    destination: SocketAddr,
}

impl fmt::Debug for Pipe {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Pipe({:?} -> {:?})", self.socket, self.destination)
    }
}

impl Pipe {
    pub fn new(socket: UdpSocket, addr: SocketAddr) -> Pipe {
        socket.set_nonblocking(true).unwrap();

        Pipe {
            socket: socket,
            destination: addr,
        }
    }

    pub fn read(&mut self, buf: &mut [u8; MAX_PACKET_SIZE]) -> Result<usize, ()> {
        let data = self.socket.recv_from(buf);
        if let Ok((len, addr)) = data {
            // TODO: Confirm valid via encryption and strip encryption.
            self.destination = addr;
            Ok(len)
        }
        else {
            // TODO: Handle interesting errors if there are any
            Err(())
        }
    }

    // Returns the actual amount of data sent, assuming normal UDP/IP headers
    pub fn write(&mut self, data: &[u8]) -> usize {
        // TODO: Encrypt
        let r = self.socket.send_to(data, self.destination);
        // TODO: Real error handling?
        assert!(r.unwrap() == data.len());

        data.len() + 28
    }
}