#[cfg(any(feature = "send", feature="recv"))]
mod pipe;
#[cfg(any(feature = "send", feature="recv"))]
pub use self::pipe::Pipe;

#[cfg(feature = "recv")]
mod recv;
#[cfg(feature = "recv")]
pub use self::recv::{Connection as RecvConnection, Event as RecvEvent};

#[cfg(feature = "send")]
mod send;
#[cfg(feature = "send")]
pub use self::send::{Connection as SendConnection, Event as SendEvent};

const MAX_PACKET_SIZE: usize = 1500;
const MAX_OUTER_DATA_SIZE: usize = MAX_PACKET_SIZE
    - 8         // UDP overhead
    - 60        // IP overhead
    ;
const MAX_DATA_SIZE: usize = MAX_OUTER_DATA_SIZE
    - 128 / 8   // OCB tag overhead (we could reduce this, 0 <= TAG_SIZE <= 128 / 8, 0 would be a really bad idea)
    - 24 / 8    // Packet length, not including the above overhead, must be a multiple of 128 bits.
                // TODO: Reducing tag length by 24 bits would allow optimal packing, good idea?
    ;

/// Max number of packets sent after a packet, than can arrive before a packet, before we consider the packet lost.
/// We only refresh for lost packets when sending Acks so some reordered packets may still end up arriving in time
/// to be used.
const MAX_REORDER: usize = 5;
