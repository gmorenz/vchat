#[cfg(feature = "trace_")]
use ::depth;

use std::fmt;
use std::cmp::max;
use super::{MAX_DATA_SIZE, MAX_PACKET_SIZE, MAX_REORDER, Pipe};

use byteorder::{LittleEndian, ReadBytesExt, WriteBytesExt};

const METADATA_SIZE: usize = 8;

#[derive(Clone, PartialEq, Eq, Debug)]
pub enum Event {
    /// We have recieved confimation that this packet arrived.
    PacketArrived(u32),
    /// We have recieved confirmation that this packet will be treated as lost.
    PacketLost(u32),
    None
}

pub struct Connection {
    // Public for diagnostic purposes only
    pub pipe: Pipe,

    // Used in handling Acking
    next_id: u32,
    packets_waiting_ack: Vec<u32>,
    packets_acked: Vec<u32>,
    packets_lost: Vec<u32>,
    highest_ack_recieved: u32,
    /// Sorted list low->high, anything lower than low in packets_waiting_ack is
    /// treated as lost. (List updates are treated as atomic on recieving an Ack)
    highest_packets_acked: [u32; MAX_REORDER], 

    next_data: [u8; MAX_DATA_SIZE],
    next_len: usize,
}

impl fmt::Debug for Connection {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Connection;")
    }
}

#[cfg_attr(feature = "trace_", trace)]
impl Connection {
    pub fn new(pipe: Pipe) -> Connection {
         Connection {
            pipe: pipe,

            next_id: 1,
            packets_waiting_ack: vec!(),
            packets_acked: vec!(),
            packets_lost: vec!(),
            highest_ack_recieved: 0,
            highest_packets_acked: [0; MAX_REORDER],

            next_data: [0; MAX_DATA_SIZE],
            next_len: METADATA_SIZE,
        }
    }

    pub fn poll(&mut self) -> Event {
        if let Some(packet) = self.packets_acked.pop() {
            return Event::PacketArrived(packet);
        }

        if let Some(packet) = self.packets_lost.pop() {
            return Event::PacketLost(packet);
        }

        self.recv_acks();

        Event::None
    }
    
    pub fn space_in_packet(&self) -> usize {
        MAX_DATA_SIZE - self.next_len
    }

    /// Adds data to send on the next outgoing packet, panics if there isn't enough space
    /// available (checked via `space_in_packet()`). Returns the id of the packet, which
    /// you should store so that you can later find out if the patch arrived succesfully.
    pub fn add_data(&mut self, data: &[u8]) -> u32 {
        let end = self.next_len + data.len();
        // TODO: Error nicely
        assert!(end <= MAX_DATA_SIZE);
        let slice = &mut self.next_data[self.next_len.. end];
        slice.copy_from_slice(data);
        self.next_len = end;

        self.next_id
    }

    // Returns an (approximated) amount of data sent over the connection
    pub fn send_now(&mut self) -> usize {
        if self.next_len == METADATA_SIZE { return 0 };

        // Stick packet_id and ack confirmation is reserved space
       {
            let mut wtr = &mut self.next_data[0..METADATA_SIZE];
            wtr.write_u32::<LittleEndian>(self.next_id).unwrap();
            wtr.write_u32::<LittleEndian>(self.highest_ack_recieved).unwrap();

            self.packets_waiting_ack.push(self.next_id);

            self.next_id += 1;
        }

        let data_sent = self.pipe.write(&self.next_data[..self.next_len]);

        // New packet
        self.next_len = METADATA_SIZE;

        data_sent
    }

    fn recv_acks(&mut self) {
        // TODO: Make sure that super::read doesn't return Err when there is one
        // 'noise' (unencrypted, not from right sender) packet with valid packets behind
        // it in the queue.
        let mut buf = [0; MAX_PACKET_SIZE];
        let len = self.pipe.read(&mut buf);
        if let Ok(len) = len {
            let mut rdr = &buf[..len];
            let ack_id = rdr.read_u32::<LittleEndian>().unwrap();
            self.highest_ack_recieved = max(self.highest_ack_recieved, ack_id);

            for _ in 0..(len / 4)-1 {
                let acked_packet = rdr.read_u32::<LittleEndian>().unwrap();
                if self.remove_waiting_ack(acked_packet) {
                    self.packets_acked.push(acked_packet);

                    let mut tmp = acked_packet;
                    for x in &mut self.highest_packets_acked {
                        if tmp >= *x {
                            use std::mem::swap;
                            swap(&mut tmp, x);
                        }
                        else {
                            break
                        }
                    }
                }
            }

            let lowest_found_packet = self.highest_packets_acked[0];
            if self.packets_waiting_ack.len() > 0 && self.packets_waiting_ack[0] < lowest_found_packet {
                // TODO: Check if this is an efficiency concern, maybe switch to deque.
                self.packets_lost.push(self.packets_waiting_ack.remove(0));
            }
        }
    }

    /// If acked_patch in self.packets_waiting_ack returns true, removes from list.
    /// Else returns false. 
    fn remove_waiting_ack(&mut self, acked_packet: u32) -> bool {
        for i in 0..self.packets_waiting_ack.len() {
            if acked_packet == self.packets_waiting_ack[i] {
                self.packets_waiting_ack.remove(i);
                return true;
            }
            else if acked_packet < self.packets_waiting_ack[i] {
                return false;
            }
        }
        return false;
    }
}