#[cfg(feature = "trace_")]
use ::depth;

use byteorder::{LittleEndian, ReadBytesExt, WriteBytesExt};

use time::precise_time_ns;

use std::collections::VecDeque;
use std::fmt;
use super::{Pipe, MAX_PACKET_SIZE, MAX_REORDER};


const ACK_RESEND_TIME_NS: u64 = 1_000_000_000;

#[derive(PartialEq, Eq, Debug)]
pub enum Event<P> {
    // TODO: Figure out how to make this reference the Connection's buffer nicely.
    RecievedPatch(P),
    None
}

pub struct Connection {
    // Public for diagnostic purposes only
    pub pipe: Pipe,

    // Used in acking
    needs_succesful_ack: VecDeque<(u32, u32)>, // PacketId, First ACK Containing packet (may be next ack)
    highest_packets_acked: [u32; MAX_REORDER],
    next_id: u32,
    last_sent_ack: u64, // Time in nanosecnds

    buffer: [u8; MAX_PACKET_SIZE],
    buffer_len: usize,
    buffer_consumed: usize,
}

impl fmt::Debug for Connection {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Connection;")
    }
}

#[cfg_attr(feature = "trace_", trace(disable(poll)))]
impl Connection {
    pub fn new(pipe: Pipe) -> Connection {
        Connection {
            pipe: pipe,

            needs_succesful_ack: VecDeque::new(),
            highest_packets_acked: [0; MAX_REORDER],
            next_id: 1,
            last_sent_ack: 0,

            buffer: [0; MAX_PACKET_SIZE],
            buffer_len: 0,
            buffer_consumed: 0
        }
    }

    pub fn poll<P, F>(&mut self, f: F ) -> Event<P> 
    where F: FnOnce(&[u8]) -> (usize, P) {
        if self.buffer_len == self.buffer_consumed {
            self.read();
        }

        if self.last_sent_ack + ACK_RESEND_TIME_NS < precise_time_ns() {
            self.send_ack();
        }

        if self.buffer_len == 0 {
            return Event::None;
        }

        let (len, patch) = f(&self.buffer[self.buffer_consumed..]);
        let remaining = self.buffer_len - self.buffer_consumed;
        // TODO: Gracefully error
        assert!(len <= remaining);

        self.buffer_consumed = self.buffer_consumed + len;

        Event::RecievedPatch(patch)
    }

    fn read(&mut self) {
        let len = self.pipe.read(&mut self.buffer);
        if let Ok(len) = len {
            // TODO: nicer error handling?
            assert!(len >= 8);

            // Handle incoming metadata

            {
                let mut rdr = &self.buffer[..8];
                let packet_id = rdr.read_u32::<LittleEndian>().unwrap();
                let confirmed_ack = rdr.read_u32::<LittleEndian>().unwrap();

                // If packet is too old throw it out (treated as lost)
                if packet_id < self.highest_packets_acked[0] {
                    // Same as top level Else (goto would be nice!)
                    self.buffer_len = 0;
                    self.buffer_consumed = 0;
                    return;
                }

                self.needs_succesful_ack.push_back((packet_id, self.next_id));
                while let Some(&(_, ack_id)) = self.needs_succesful_ack.front() {
                    if ack_id <= confirmed_ack {
                        self.needs_succesful_ack.pop_front();
                    }
                    else {
                        break;
                    }
                }
            }

            // Send another Ack

            self.send_ack();

            // Set up buf sizes

            self.buffer_len = len;
            self.buffer_consumed = 8;
        }
        else {
            // Same as in case of packet_id < self.highest_packets_acked[0] (goto would be nice!)
            // TODO: Handle interesting errors if there are any
            self.buffer_len = 0;
            self.buffer_consumed = 0;
        }
    }

    fn send_ack(&mut self) {
        if self.needs_succesful_ack.len() == 0 {
            return;
        }

        let mut data = [0; MAX_PACKET_SIZE];
        let mut data_len = 0;
        {
            let mut wtr = &mut data[..];
            wtr.write_u32::<LittleEndian>(self.next_id).unwrap();
            self.next_id += 1;
            data_len += 4;
            for &(packet, _) in &self.needs_succesful_ack {
                // If we have more packets to Ack than fit, then only Ack the first group.
                if let Err(_) = wtr.write_u32::<LittleEndian>(packet) {
                    break;
                }
                data_len += 4;

                let mut tmp = packet;
                for x in &mut self.highest_packets_acked {
                    use std::mem::swap;
                    if tmp > *x {
                        swap(&mut tmp, x);
                    }
                }
            }
        }

        // TODO: Should I make sure this call ignores rerors?
        self.pipe.write(&data[..data_len]);

        self.last_sent_ack = precise_time_ns();
    }
}