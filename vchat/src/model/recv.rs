use super::Patch;

use image::Image;

pub struct Model {
    pub image: Box<Image>
}

impl Model {
    pub fn new() -> Model {
        Model {
            image: box Image::new()
        }
    }
    
    pub fn apply_patch(&mut self, patch: Patch) {
        self.image.apply_patch(patch.block_id as usize, patch.block_data);
    }
}