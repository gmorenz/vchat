#[allow(dead_code)]
#[path="../../../arithmancy/src/lib.rs"]
mod arithmancy;

#[cfg(feature = "trace_")]
use ::depth;

#[cfg(feature = "send")]
mod send;
#[cfg(feature = "send")]
pub use self::send::Model as SendModel;

#[cfg(feature = "recv")]
mod recv;
#[cfg(feature = "recv")]
pub use self::recv::Model as RecvModel;

use std::mem;

use bitvec::BitVec;
use byteorder::{LittleEndian, ReadBytesExt, WriteBytesExt};
use self::arithmancy::{Encoder, Decoder};
use self::arithmancy::cauchy_distribution::TruncatedCauchyDistribution as Cauchy;

use image::{WIDTH, HEIGHT, BLOCK_SIZE, OwnedBlock};

pub const NUM_OBJECTS: usize = 3 * WIDTH / BLOCK_SIZE * HEIGHT / BLOCK_SIZE;

// TODO: Update NUM_ENCODED_OBJECTS to be greater again ;)
/// The number of objects we save space for in the 16 bit header of packets.
/// Block ID's are taken by taking the first 16 bits of a packet and modding
/// it by this number. This is greater than the NUM_OBJECTS because it can because
/// without affecting the rest of the data we store in the header, so we might as
/// well reserve some special objects.
const NUM_ENCODED_OBJECTS: u16 = NUM_OBJECTS as u16;
// Note: This constant is currently not respected, which is only OK because it's 0
#[allow(dead_code)]
const FIXED_WIDTH_PATCH: u16 = 0;
const AERITH_CODED_PATCH: u16 = 1;

#[derive(Copy, Clone, Debug, PartialEq)]
#[repr(C)] // Since we transmute from this we need to know it is packed as expected
pub struct Patch{
    pub block_id: u16,
    block_data: OwnedBlock<u8>
}

#[cfg_attr(feature = "trace_", trace)]
impl Patch {
    pub fn from_bytes(data: &[u8]) -> (usize, Patch) {
        let mut rdr = data;
        let header: u16 = rdr.read_u16::<LittleEndian>().unwrap();
        match header / NUM_ENCODED_OBJECTS {
            FIXED_WIDTH_PATCH => Patch::from_fixed_width_bytes(data),
            AERITH_CODED_PATCH => Patch::from_aerith_bytes(data),
            _ => panic!("Invalid patch header: {}", header)
        }
    }

    pub fn to_bytes(&self) -> Vec<u8> {
        let aerith = self.to_aerith_bytes();
        let ret =
        if aerith.len() > 2 + BLOCK_SIZE * BLOCK_SIZE {
            self.to_fixed_width_bytes()
        }
        else {
            aerith
        };

        debug_assert!(Patch::from_bytes(&ret).1 == *self);

        ret
    }

    fn from_fixed_width_bytes(data: &[u8]) -> (usize, Patch) {
        let mut rdr = data;
        let block_id = rdr.read_u16::<LittleEndian>().unwrap() % NUM_ENCODED_OBJECTS;

        let len = ::image::BLOCK_SIZE.pow(2) + 2;
        assert_eq!(len, 2 + mem::size_of::<OwnedBlock<u8>>());
        assert!(data.len() >= len);

        (len, Patch {
            block_id: block_id,
            block_data: OwnedBlock::<u8>::from_bytes(&data[2..len])
        })
    }

    fn to_fixed_width_bytes(&self) -> Vec<u8> {
        assert_eq!(mem::size_of::<Patch>(), 2 + mem::size_of::<OwnedBlock<u8>>());
        const SIZE: usize = 2 + BLOCK_SIZE * BLOCK_SIZE;
        assert_eq!(mem::size_of::<Patch>(), SIZE);
        let bytes: &[u8; SIZE] = unsafe{ mem::transmute( self ) };
        bytes.to_vec()
    }

    fn from_aerith_bytes(data: &[u8]) -> (usize, Patch) {
        /*
        print!("recv: [ {}", data[0]);
        for d in &data[1..] {
            print!(", {}", d);
        }
        print!("]");
        */

        let mut rdr = data;
        let block_id = rdr.read_u16::<LittleEndian>().unwrap() % NUM_ENCODED_OBJECTS;

        // println!(" : {}", block_id);

        let len = rdr.read_u16::<LittleEndian>().unwrap() as usize;
        // TODO: Figure this out during decoding instead of transmitting.
        let byte_len = len / 8 + if len % 8 == 0 { 0 } else { 1 };

        const SIZE: usize = BLOCK_SIZE * BLOCK_SIZE;
        let mut decoded = [0; SIZE];
        {
            use ::bititer::BitIter;
            let data_iter = BitIter {
                data: data,
                index: 32
            };

            let mut model = Cauchy{ x0: 0, gamma: 16. };
            let mut decoder = Decoder::new(data_iter);
            for i in 0.. SIZE {
                decoded[i] = decoder.next(&model);
                model = Cauchy{ x0: decoded[i], gamma: 0.5 };
            }
        }
        (byte_len, Patch {
            block_id: block_id,
            block_data: OwnedBlock::<u8>::from_bytes(&decoded)
        })
    }

    fn to_aerith_bytes(&self) -> Vec<u8> {
        let mut ret = vec!();
        ret.write_u16::<LittleEndian>(
            self.block_id + AERITH_CODED_PATCH * NUM_ENCODED_OBJECTS).unwrap();
        ret.write_u16::<LittleEndian>(0).unwrap(); // Reserve space for length

        // TODO: Much better model
        let mut model = Cauchy{ x0: 0, gamma: 16. };
        let mut compressed = BitVec::from_raw(ret, 32);

        const SIZE: usize = BLOCK_SIZE * BLOCK_SIZE;
        assert_eq!(SIZE, mem::size_of::<OwnedBlock<u8>>());
        let bytes: &[u8; SIZE] = unsafe{ mem::transmute(&self.block_data) };

        let mut encoder = Encoder::new();
        for i in 0.. SIZE {
            encoder.encode(&model, |bit| compressed.push(bit), bytes[i]);
            model = Cauchy{ x0: bytes[i], gamma: 0.5 };
        }
        encoder.finalize(|bit| compressed.push(bit));

        let len = compressed.len();

        let mut ret = compressed.into_vec();
        // TODO: Have some form of return invalid.
        assert!(len <= 0xffff);
        (&mut ret[2..]).write_u16::<LittleEndian>(len as u16).unwrap();
        ret
    }
}