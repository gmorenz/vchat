#![allow(dead_code)]

use super::{Patch, NUM_OBJECTS};
use image::{Image, OwnedBlock};
use std::mem;
use std::fmt;

#[cfg(feature = "trace")]
#[allow(unused_imports)]
use ::depth;


pub struct Model {
    pub target_image: Box<Image>,
    pub client_image: Box<Image>,
    patches_in_flight: [Option<OwnedBlock<u8>>; NUM_OBJECTS],

    // Used in optimization
    patch_list_updated: bool,
    patch_list: Box<[OwnedBlock<u8>; NUM_OBJECTS]>,
    patch_order: Vec<(usize, usize)> // Error, Patch #

}

impl fmt::Debug for Model {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Model({:?}, {:?})", self.target_image, self.client_image)
    }
}

// #[trace]
impl Model {
    pub fn new() -> Model {
        Model {
            target_image: box Image::new(),
            client_image: box Image::new(),
            patches_in_flight: [None; NUM_OBJECTS],
            patch_list_updated: false,
            patch_list: box [OwnedBlock::zero(); NUM_OBJECTS],
            patch_order: Vec::with_capacity(NUM_OBJECTS)
        }
    }

    /// Creates a patch, records it as a patch in flight
    pub fn diff(&mut self) -> Option<Patch> {
        if !self.patch_list_updated {
            self.update_patch_list();
        }

        let best_patch = self.patch_order.pop();

        if let Some((_, block_id)) = best_patch {
            debug_assert!(block_id < 3600);
            let block_data = self.patch_list[block_id];

            let patch = Some(Patch {
                block_id: block_id as u16,
                block_data: block_data
            });

            self.patches_in_flight[block_id] = Some(block_data);
            patch
        }
        else {
            // TODO: This only happens if we have a patch in flight for every block,
            // may be worthwhile to consider doing something more intelligent here.
            None
        }
    }

    /// Applies a patch, removes it from patch in flight
    pub fn apply_client_patch(&mut self, id: u16) {
        let data = self.patches_in_flight[id as usize].unwrap();
        self.client_image.apply_patch(id as usize, data);
        self.patches_in_flight[id as usize] = None;

        // TODO: Update patch_list, only recalculating the object applied to and inserting
    }

    pub fn forget_client_patch(&mut self, id: u16) {
        self.patches_in_flight[id as usize] = None;
    }

    pub fn update_local_yuyv480(&mut self, data: &[u8]) {
        self.target_image.setdata_yuyv480_ycbcr(data);
        self.patch_list_updated = false;
    }

    fn update_patch_list(&mut self) {

        let mut patch_list = mem::replace(&mut self.patch_list,
            unsafe{ mem::uninitialized() });
        let mut patch_order = mem::replace(&mut self.patch_order, vec!());

        {
            self.patch_list_updated = true;
            patch_order.clear();
            let patch_iter =
                // Iterate through blocks
                self.target_image.block_iter()
                // Associate the target blocks with the client blocks
                .zip(self.client_image.block_iter())
                // Attach block ids
                .enumerate()
                // Remove blocks with patches in flight
                .filter(|&(id, _)| self.patches_in_flight[id].is_none())
                // Calculate patch data
                .map(|(id, (src_block, dst_block))| {
                    let diff = src_block - dst_block;
                    let err = error(diff);
                    (id, diff.into_u8_block(), err)
                });

            for (id, diff, err) in patch_iter {
                patch_list[id] = diff;
                patch_order.push((err, id));
            }

            patch_order.sort();
        }

        let uninit_list = mem::replace(&mut self.patch_list, patch_list);
        mem::forget(uninit_list);
        self.patch_order = patch_order;
    }
}

fn error(diff: OwnedBlock<i16>) -> usize {
    diff.sum_squared_value()
    /* Experimental error by most error in a 8x8 corner,
     *  poor results, no emphasis on updating interior of limbs...
    let mut err = 0;
    for vert_half in 0..2 {
        for horiz_half in 0..2 {
            let mut quarter_err = 0;
            for i in 0..BLOCK_SIZE/2 {
                for j in 0..BLOCK_SIZE/2 {
                    for p in diff[i + vert_half * BLOCK_SIZE/2]
                                 [j + horiz_half * BLOCK_SIZE/2].iter() {
                        quarter_err += p.pow(2) as usize;
                    }
                }
            }
            use std;
            err = std::cmp::max(err, quarter_err);
        }
    }

    err
    */
}