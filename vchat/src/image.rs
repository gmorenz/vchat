
use std::ops::{Deref, DerefMut, Sub};
use std::fmt;
use std::mem;

pub const WIDTH: usize = 640;
pub const HEIGHT: usize = 480;
pub const BLOCK_SIZE: usize = 16;

pub struct Image {
    y: [[u8; WIDTH]; HEIGHT],
    cb: [[u8; WIDTH]; HEIGHT],
    cr: [[u8; WIDTH]; HEIGHT]
}

impl fmt::Debug for Image {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        // TODO: Print data
        write!(f, "Image( some data )")
    }
}

impl Image {
    pub fn new() -> Image {
        Image {
            y: [[0; WIDTH]; HEIGHT],
            cb: [[0; WIDTH]; HEIGHT],
            cr: [[0; WIDTH]; HEIGHT]
        }
    }

    // Sets image data to a YCbCr 'uncompressed' version of the YUYV data.
    pub fn setdata_yuyv480_ycbcr(&mut self, data: &[u8]) {
        assert!(data.len() == WIDTH * HEIGHT * 2);

        for i in 0 .. HEIGHT {
            for j in 0 .. WIDTH {
                let index = i * WIDTH + j;
                self.y[i][j] = data[index * 2];
                self.cb[i][j] = data[index * 2 + 1 - 2 * (index % 2)];
                self.cr[i][j] = data[index * 2 + 3 - 2 * (index % 2)];
            }
        }
    }

    pub fn apply_patch(&mut self, id: usize, data: OwnedBlock<u8>) {
        let start_width = ((id % 1200) * BLOCK_SIZE) % WIDTH;
        let start_height = ((id % 1200) * BLOCK_SIZE / WIDTH) * BLOCK_SIZE;

        let channel = self.get_channel_mut(id);

        for row in 0.. BLOCK_SIZE {
            for offset in 0..BLOCK_SIZE {
                channel[start_height + row][start_width + offset]
                    = channel[start_height + row][start_width + offset]
                    .wrapping_add(data[row][offset])
            }
        }
    }

    pub fn block_iter(&self) -> BlockIter {
        BlockIter {
            image: self,
            id: 0
        }
    }

    pub fn as_ptrs(&self) -> [*const u8; 3] {
        [self.y.as_ptr() as *const _, self.cb.as_ptr() as *const _, self.cr.as_ptr() as *const _]
    }

    fn get_channel_mut(&mut self, id: usize) -> &mut [[u8; WIDTH]; HEIGHT] {
        let channel_index = id / 1200;

        match channel_index {
            0 => &mut self.y,
            1 => &mut self.cb,
            2 => &mut self.cr,
            _ => panic!("Invalid id")
        }
    }

    fn get_channel(&self, id: usize) -> &[[u8; WIDTH]; HEIGHT] {
        let channel_index = id / 1200;

        match channel_index {
            0 => &self.y,
            1 => &self.cb,
            2 => &self.cr,
            _ => panic!("Invalid id")
        }
    }
}

/* TODO: Remove
impl Deref for Image {
    type Target = [[u8; 3]; WIDTH * HEIGHT];
    fn deref(&self) -> &[[u8; 3]; WIDTH * HEIGHT] {
        &self.data
    }
}

impl DerefMut for Image {
    fn deref_mut(&mut self) ->  &mut [[u8; 3]; WIDTH * HEIGHT] {
        &mut self.data
    }
}
*/

#[derive(Clone, Copy)]
pub struct Block<'a> {
    // TODO: Strip this down to a single pointer, or pointer and index at most
    data: [&'a [u8; BLOCK_SIZE]; BLOCK_SIZE],
}

impl <'a> Deref for Block<'a> {
    type Target = [&'a [u8; BLOCK_SIZE]; BLOCK_SIZE];
    fn deref(&self) -> &[&'a [u8; BLOCK_SIZE]; BLOCK_SIZE] {
        &self.data
    }
}

impl <'a> Sub for Block<'a> {
    type Output = OwnedBlock<i16>;
    fn sub(self, rhs: Block<'a>) -> OwnedBlock<i16> {
        let mut ret = OwnedBlock([[0; BLOCK_SIZE]; BLOCK_SIZE]);

        for i in 0..BLOCK_SIZE {
            for j in 0..BLOCK_SIZE {
                ret[i][j] = (self[i][j] as i16) - (rhs[i][j] as i16);
            }
        }

        ret
    }
}

impl <'a> Block<'a> {
    fn new(image: &'a Image, id: usize) -> Block {
        let start_width = ((id % 1200) * BLOCK_SIZE) % WIDTH;
        let start_height = (((id % 1200)* BLOCK_SIZE) / WIDTH) * BLOCK_SIZE;
        let channel = image.get_channel(id);

        let mut data: [&'a [u8; BLOCK_SIZE]; BLOCK_SIZE] = unsafe{ mem::uninitialized() };
        for row in 0..BLOCK_SIZE {
            // TODO: Is there really no easier way to convert to a fixed width slice
            let slice: &[u8; 16] = unsafe { mem::transmute({
                let slice = channel[start_height + row].split_at(start_width).1;
                slice.as_ptr()
            })};
            data[row] = slice;
        }

        Block{ data: data }
    }
}

#[derive(Copy, Clone, PartialEq)]
#[repr(C)] // Since we transmute from this we need to know it's packed as expected
pub struct OwnedBlock<T: Copy>([[T; BLOCK_SIZE]; BLOCK_SIZE]);

impl<T: Copy> fmt::Debug for OwnedBlock<T> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        // TODO: At least write what T is.
        write!(f, "OwnedBlock<T>")
    }
}

impl <T: Copy> Deref for OwnedBlock<T> {
    type Target = [[T; BLOCK_SIZE]; BLOCK_SIZE];
    fn deref(&self) -> &[[T; BLOCK_SIZE]; BLOCK_SIZE] {
        &self.0
    }
}

impl <T: Copy> DerefMut for OwnedBlock<T> {
    fn deref_mut(&mut self) -> &mut [[T; BLOCK_SIZE]; BLOCK_SIZE] {
        &mut self.0
    }
}

impl OwnedBlock<u8> {
    pub fn from_bytes(data: &[u8]) -> OwnedBlock<u8> {
        assert_eq!(data.len(), mem::size_of::<OwnedBlock<u8>>());
        let mut inner = [0; BLOCK_SIZE * BLOCK_SIZE];
        inner.copy_from_slice(data);
        let shaped: [[u8; BLOCK_SIZE]; BLOCK_SIZE] = unsafe {
            mem::transmute(inner)
        };

        OwnedBlock(shaped)
    }
}

impl OwnedBlock<i16> {
    #[allow(dead_code)]
    pub fn sum_absolute_value(&self) -> usize {
        let mut acc = 0;
        for i in 0..BLOCK_SIZE {
            for j in 0..BLOCK_SIZE {
                unsafe {
                    acc += self
                        .get_unchecked(i)
                        .get_unchecked(j)
                        .abs() as usize;
                }
            }
        }

        acc
    }

    #[allow(dead_code)]
    pub fn sum_squared_value(&self) -> usize {
        let mut acc = 0;
        for i in 0..BLOCK_SIZE {
            for j in 0..BLOCK_SIZE {
                unsafe {
                    // Cast to i32 so that 255^2 doesn't overflow the int (thanks int overflow checks!)
                    acc += (*self
                        .get_unchecked(i)
                        .get_unchecked(j)
                            as i32).pow(2) as usize;
                }
            }
        }

        acc
    }

    pub fn into_u8_block(&self) -> OwnedBlock<u8> {
        let mut ret = OwnedBlock::zero();
        for i in 0..BLOCK_SIZE {
            for j in 0..BLOCK_SIZE {
                ret[i][j] = self[i][j] as u8;
            }
        }

        ret
    }
}

// These give nonsense deprecated errors about Iterator::sum
#[allow(deprecated)]
use std::num::Zero;
#[allow(deprecated)]
impl <T: Copy + Zero> OwnedBlock<T> {
    pub fn zero() -> OwnedBlock<T> {
        OwnedBlock([[T::zero(); BLOCK_SIZE]; BLOCK_SIZE])
    }
}

pub struct BlockIter<'a> {
    image: &'a Image,
    id: usize,
}

impl <'a> Iterator for BlockIter<'a> {
    type Item = Block<'a>;
    fn next(&mut self) -> Option<Block<'a>> {
        if self.id >= 3 * WIDTH * HEIGHT / BLOCK_SIZE / BLOCK_SIZE { None }
        else {
            let ret = Block::new(&self.image, self.id);
            self.id += 1;

            Some(ret)
        }
    }
}