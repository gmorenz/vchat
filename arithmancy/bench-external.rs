#![feature(plugin, custom_attribute, test, rand)]

extern crate test;
extern crate rand;

use self::test::{black_box, Bencher};
use self::rand::Rng;

extern crate arithmancy;
use arithmancy::{encode, AdaptiveFrequencyModel};

#[bench]
fn simple_encode(b: &mut Bencher) {
    let mut data = [0; 256];
    let mut rng = rand::IsaacRng::new_unseeded();
    for i in 0..256 {
        data[i] = rng.gen::<u8>();
    }
    b.iter(|| {
        let mut model = AdaptiveFrequencyModel::new();
        encode(&mut model, data.iter().map(|x| *x), |bit| {black_box(bit);});
    })
}