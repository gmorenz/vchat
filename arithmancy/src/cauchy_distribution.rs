use std::intrinsics::{fadd_fast, fsub_fast, fmul_fast, fdiv_fast};

use super::cdf_model::TruncatedCdfModel;
use std::f64::consts::PI;

pub struct TruncatedCauchyDistribution {
    pub x0: u8,
    pub gamma: f64
}

impl TruncatedCdfModel for TruncatedCauchyDistribution {
    fn tmean(&self) -> u8 { self.x0 }

    fn untruncated_cdf(&self, val: f64) -> f64 {
        // ((val)/self.gamma).atan() / PI + (1.0/2.0)
        unsafe {
            fadd_fast(fdiv_fast(fdiv_fast(val, self.gamma).atan(), PI), 0.5)
        }
    }

    fn untruncated_icdf(&self, val: f64) -> f64 {
        // (PI * (val - (1.0/2.0))).tan() * self.gamma
        unsafe {
            fmul_fast(fmul_fast(PI, fsub_fast(val, 0.5)).tan(), self.gamma)
        }
    }
}