use super::*;

fn encode_decode_test(data: &str) -> f64 {
    let mut in_model = super::AdaptiveFrequencyModel::new();
    let mut out_model = in_model.clone();
    let mut compressed = vec!();

    let mut encoder = Encoder::new();
    for c in data.chars() {
        encoder.encode(&mut in_model, |bit| compressed.push(bit), c as u8);
        in_model.update(c as u8);
    }
    encoder.finalize(|bit| compressed.push(bit));

    print!("\ncompressed: ");
    for &b in &compressed {
        let b = if b { 1 } else { 0 };
        print!("{}", b);
    }
    println!("\n");
    let compressed_len = compressed.len();

    let mut decoder = Decoder::new(compressed.into_iter());
    for c in data.chars() {
        assert_eq!(c as u8, decoder.next(&mut out_model));
        out_model.update(c as u8);
    }

    return data.len() as f64 * 8. / compressed_len as f64;
}

fn encode_decode_test_u8(data: &[u8]) -> f64 {
    let mut in_model = super::AdaptiveFrequencyModel::new();
    let mut out_model = in_model.clone();
    let mut compressed = vec!();

    let mut encoder = Encoder::new();
    for &c in data {
        encoder.encode(&mut in_model, |bit| compressed.push(bit), c);
        in_model.update(c);
    }
    encoder.finalize(|bit| compressed.push(bit));

    let compressed_len = compressed.len();

    let mut decoder = Decoder::new(compressed.into_iter());
    for &x in data {
        assert_eq!(x, decoder.next(&mut out_model));
        out_model.update(x);
    }

    data.len() as f64 * 8. / compressed_len as f64
}

#[test]
fn basic_pass_8() {
    let data = [0; 5];
    encode_decode_test_u8(&data);
}

#[test]
fn basic_pass_7() {
    let data = [255; 4096];
    encode_decode_test_u8(&data);
}

/* Time Consuming
#[test]
fn adaptive_renormalize() {
    let mut frequencies: Vec<u32> = (0..255).collect();
    frequencies.push(0x7fffffff);

    let data = [255; 4096];
    let mut in_model = super::AdaptiveFrequencyModel::from_frequencies(frequencies);
    let mut out_model = in_model.clone();
    let mut compressed = vec!();
    for _ in 0..1024 {
        println!("cycle");
        encode(&mut in_model, data.iter().map(|&x| x), |bit| compressed.push(bit));
        let mut decoded = 0;
        println!("coding {:?}", compressed);
        decode(&mut out_model, compressed.iter().map(|&x| x), |byte| {
            assert_eq!(byte, 255);
            decoded += 1;
            decoded == 4096
        });
        println!("in_model: {:?}", in_model);
        println!("out_model: {:?}", out_model);
        compressed.clear();
    }
}
*/

#[test]
fn basic_pass_1() {
    let ratio = encode_decode_test("abccbaaaaaaaaaac");


    println!("ratio: {}", ratio);
    assert!(ratio >= 1.2190476190476192);
}

#[test]
fn basic_pass_2() {
    let ratio = encode_decode_test("a");


    println!("ratio: {}", ratio);
    assert!(ratio >= 0.8);
}

#[test]
fn basic_pass_3() {
    let ratio = encode_decode_test("aba");


    println!("ratio: {}", ratio);
    assert!(ratio >= 0.96);
}

#[test]
fn basic_pass_4() {
    let ratio = encode_decode_test("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");


    println!("ratio: {}", ratio);
    assert!(ratio >= 2.368);
}

#[test]
fn basic_pass_5() {
    let ratio = encode_decode_test("bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb");


    println!("ratio: {}", ratio);
    assert!(ratio >= 2.34375);
}

#[test]
fn basic_pass_6() {
    let ratio = encode_decode_test("cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaab");

    println!("ratio: {}", ratio);
    assert!(ratio >= 2.262983425414365 );
}

#[test]
fn invalid_symbol_via_vchat() {
    let data = [ 84, 0];
    encode_decode_test_u8(&data);
}