#![cfg_attr(all(test, feature = "standalone_arithmancy"), feature(test, rand))]
#![cfg_attr(feature = "standalone_arithmancy", feature(core_intrinsics))]
#[cfg(all(test, feature = "standalone_arithmancy"))]
mod test;

#[cfg(all(test, feature = "standalone_arithmancy"))]
mod bench;

use std::fmt;

pub mod normal_distribution;
pub mod cauchy_distribution;
pub mod cdf_model;

mod model;
pub use self::model::{Model, AdaptiveFrequencyModel};

pub const MIN_RANGE: u32 = 0xC0000000 - 0x7FFFFFFF;

const HIGH_BIT_ONE: u32 = 0x80000000;

#[derive(Clone, Copy)]
pub struct Interval {
    pub lower: u32,
    pub upper: u32,
}

impl fmt::Debug for Interval {
    fn fmt(&self, out: &mut fmt::Formatter) -> fmt::Result {
        write!(out, "Inteval{{ lower: {:08x}, upper: {:08x} }}", self.lower, self.upper)
    }
}

#[derive(Clone, Copy)]
struct SearchInterval {
    lower: u32,
    upper: u32,
}

impl fmt::Debug for SearchInterval {
    fn fmt(&self, out: &mut fmt::Formatter) -> fmt::Result {
        write!(out, "SearchInteval{{ lower: {:08x}, upper: {:08x} }}", self.lower, self.upper)
    }
}

impl SearchInterval {
    fn limit_to(&mut self, interval: Interval) {
        debug_assert!(self.lower + interval.upper <= self.upper);
        debug_assert!(interval.lower <= interval.upper);

        self.upper = self.lower + interval.upper;
        self.lower = self.lower + interval.lower;
    }

    fn bits_agree(&self) -> bool {
        // (Note that we can shorten the condition since we know upper > lower)
        self.lower & HIGH_BIT_ONE != 0 || self.upper & HIGH_BIT_ONE == 0
    }

    // When bits agree this returns the last (agreed upon) bit. The rest of the
    // time this should be considered to return an undefined value.
    fn last_bit(&self) -> bool {
        self.lower & HIGH_BIT_ONE != 0
    }

    fn drop_bit(&mut self) {
        self.lower = self.lower << 1;
        self.upper = (self.upper << 1) | 1;
        // println!("drop bit search");
    }

    fn increase_range(&mut self) -> bool {
        const MIN_LOWER: u32 = (0b0100) << 28;
        const MAX_UPPER: u32 = (0b1011 << 28) |  0xFFFFFFF;
        if self.lower >= MIN_LOWER && self.upper <= MAX_UPPER  {
            // println!("increasing range");
            self.lower = (self.lower << 1) & !HIGH_BIT_ONE;
            self.upper = (self.upper << 1) | HIGH_BIT_ONE;

            true
        }
        else {
            false
        }
    }

    fn range(&self) -> u32 {
        self.upper - self.lower
    }
}

pub struct Encoder {
    search_interval: SearchInterval,
    pending_bits: u32,
}

impl Encoder {
    pub fn new() -> Encoder {
        Encoder {
            search_interval: SearchInterval {
                lower: 0,
                upper: 0xFFFFFFFF,
            },
            pending_bits: 0
        }
    }

    pub fn encode<S, M, E>(&mut self, model: &M, mut emit: E, sym: S)
    where E: FnMut(bool), S: Clone + Copy, M: Model<S> {
        // It is valid for the model to return any non-interval interval
        // which is a subset of [0, range], where range = upper - lower.
        let interval = model.get_interval(sym, self.search_interval.range());
        self.search_interval.limit_to(interval);


        // While the last bits of `lower` and `upper` agree
        // (Note that we can shorten the condition since we know upper > lower)
        while self.search_interval.bits_agree() {
            // Emit that last bit
            emit( self.search_interval.last_bit() );
            while self.pending_bits > 0 {
                emit( ! self.search_interval.last_bit() );
                self.pending_bits -= 1;
            }

            self.search_interval.drop_bit();
        }

        while self.search_interval.increase_range() {
            self.pending_bits += 1;
        }
    }

    pub fn finalize<E>(mut self, mut emit: E)
    where E: FnMut(bool) {
        /*
        We now need to make the last output symbols unambiguous. The acceptable values
        to end with are x such that lower <= x <= lower + range. (Which, if you consider
        range to continue forever with more 1s is equivalent to lower <= x < lower + range).

        There are two possible (sets of really) shortest ways to do this. Output a 0, then we
        will always be lower than upper, then keep outputing 1s until we encounter a
        0 in lower (at which point we will always be greater). Or to output a 1, and then keep
        outputing 0s until we encounter a 1 in upper. Since I'm more worried about
        a short encoding than a fast encoding I first determine which is shorter.


        */

        let mut bit_type = false;
        for i in 1..31 {
            let mask = HIGH_BIT_ONE >> i;
            if mask & self.search_interval.upper == 0 {
                bit_type  = true;
                break;
            }

            if mask & !self.search_interval.lower == 0 {
                bit_type = false;
                break;
            }
        }


        // println!("tail end bit type: {}", bit_type);
        emit( bit_type );
        while self.pending_bits > 0 {

            // println!("pending bit");
            emit( !bit_type );
            self.pending_bits -= 1;
        }
        self.search_interval.drop_bit();
        while (bit_type && (self.search_interval.upper & HIGH_BIT_ONE == 0))
        || (!bit_type && (self.search_interval.lower & HIGH_BIT_ONE != 0)) {

            // println!("loop emit");
            // println!("emit at end: {}, {:08x}", bit_type, search_interval.lower);
            emit( !bit_type );
            self.search_interval.drop_bit();
        }
        emit( !bit_type );
        // TODO: Make sure that this never includes extraneous bits :(
        // I'm particularly thinking of cases where 1 bit might be enough?
    }
}

struct DeterminedInterval {
    min: u32,
    determined: u8
}

impl DeterminedInterval {
    fn max(&self) -> u32 {
        // As u64 since shifting right 32 on a 32 bit integer doesn't work.
        let ones = ((!0u32 as u64) >> ( self.determined as u32 )) as u32;
        // println!("ones: {:08x}, determined: {}", ones, self.determined);
        self.min | ones
    }

    fn drop_bit(&mut self) {
        // println!("determined drop bit");
        self.min = self.min << 1;
        self.determined -= 1;
    }

    fn add_bit(&mut self, bit: bool) {
        debug_assert!(self.determined < 32);
        let bit = if bit {1} else {0};

        // println!("  val read: {}", bit);
        self.min = self.min | (bit << (31 - self.determined));
        self.determined += 1;
    }
}

pub struct Decoder<I: Iterator<Item=bool>> {
    inp: I,
    search_interval: SearchInterval,
    val: DeterminedInterval,
    pending_bits: u32,
}

impl <I: Iterator<Item=bool>> Decoder<I> {
    pub fn new(inp: I) -> Decoder<I> {
        Decoder {
            inp: inp,
            search_interval: SearchInterval {
                lower: 0,
                upper: 0xFFFFFFFF,
            },
            val: DeterminedInterval {
                min: 0,
                determined: 0,
            },
            pending_bits: 0
        }
    }

    pub fn next<S, M>(&mut self, model: &M) -> S
    where S: Clone + Copy, M: Model<S> {
        loop {
            // Read as much data as possible
            while self.val.determined < 32 {
                if let Some(x) = self.inp.next() {
                    self.val.add_bit(x)
                }
                else {
                    break
                }
            }

            // Get the symbol that includes the lower range of possible values, as well as the interval
            // that contains it.
            let (symbol, interval)
                = model.get_symbol_range(self.val.min - self.search_interval.lower,
                                        self.search_interval.range());

            // If we know enough to output a symbol, output it.
            if self.val.max() - self.search_interval.lower <= interval.upper {
                // Do the same lower, upper, and range increase calculations as in
                // encode, to keep the range the same for model.
                self.search_interval.limit_to(interval);
                // While the last bits of `lower` and `upper` agree
                // (Note that we can shorten the condition since we know upper > lower)
                while self.search_interval.bits_agree() {
                    // We also have to deal with pending bits here... to match encode
                    while self.pending_bits > 0 {
                        self.pending_bits -= 1;
                    }

                    self.search_interval.drop_bit();
                    self.val.drop_bit();
                }

                // The same pending bits code as above
                while self.search_interval.increase_range() {
                    self.pending_bits += 1;

                    while self.val.determined < 2 {
                        // TODO: Out of input? Possible?
                        self.val.add_bit(self.inp.next().unwrap());
                    }
                    let bit = self.val.min & HIGH_BIT_ONE;
                    debug_assert!( (self.val.min & 0x40000000 == 0) != (bit == 0) );
                    self.val.min = ((self.val.min << 1) & 0x7FFFFFFF) | bit;
                    self.val.determined -= 1;
                }

                return symbol
            }
            else {
                panic!("Non unique next symbol");
            }
        }
    }
}
