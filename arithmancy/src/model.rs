use super::{Interval, MIN_RANGE};

pub trait Model<S: Clone + Copy> {
    fn get_interval(&self, sym: S, range: u32) -> Interval;
    fn get_symbol(&self, value: u32, range: u32) -> S;
    /// Returns the symbol with an interval containing lower, as well as the size of that interval.
    fn get_symbol_range(&self, lower: u32, range: u32) -> (S, Interval) {
        let sym = self.get_symbol(lower, range);
        (sym, self.get_interval(sym, range))
    }
}

pub struct AdaptiveFrequencyModel {
    frequencies: [u32; 256],
}

impl Clone for AdaptiveFrequencyModel {
    fn clone(&self) -> AdaptiveFrequencyModel {
        let frequencies = self.frequencies;
        AdaptiveFrequencyModel {
            frequencies: frequencies
        }
    }
}

impl AdaptiveFrequencyModel {
    pub fn new() -> AdaptiveFrequencyModel {
        let mut ret = AdaptiveFrequencyModel {
            frequencies: [0; 256]
        };
        for i in 0..256 {
            ret.frequencies[i] = i as u32;
        }

        ret
    }

    pub fn update(&mut self, sym: u8) {
        let index = sym as usize;
        for val in &mut self.frequencies[index..] {
            *val += 1;
        }

        if *self.frequencies.last().unwrap() > MIN_RANGE {
            use std::cmp::max;
            // Renormalizing, dividing frequencies by two but maintaining minimum 1.
            // TODO: Make this path cold?
            let mut min_val = 0;
            let mut original_last = 0;
            let mut new_last = 0;
            for val in self.frequencies.iter_mut() {
                let size = *val - original_last;
                original_last = *val;
                *val = max(min_val, new_last + size / 2);
                new_last = *val;
                min_val = new_last + 1;
            }
        }

        // TODO: Renormalize frequencies if last elem is bigger than
        // max range
    }

    #[allow(dead_code)]
    pub fn from_frequencies(frequencies: [u32; 256]) -> AdaptiveFrequencyModel {
        AdaptiveFrequencyModel {
            frequencies : frequencies
        }
    }

    #[allow(dead_code)]
    pub fn into_frequencies(self) -> [u32; 256] {
        self.frequencies
    }
}

impl Model<u8> for AdaptiveFrequencyModel {
    // Returns the Interval containing sym out of the interval [0, range].
    fn get_interval(&self, sym: u8, range: u32) -> Interval {
        let max_range = *self.frequencies.last().unwrap();

        // println!("\tmax range: {:08x}", max_range);
        debug_assert!(range >= max_range);
        let index = sym as usize;
        // TODO: This wastes up to half the range, use a smarter technique
        let multiplier = range / max_range;

        let lower =
            if index == 0 {
                0
            }
            else {
                self.frequencies[index - 1] * multiplier + 1
            };

        let upper = self.frequencies[index] * multiplier;

        let ret = Interval {
            lower: lower,
            upper: upper
        };

        // println!("\tget_interval(sym: {}, range: {:08x}) -> {:?}", sym, range, ret);
        ret
    }

    fn get_symbol(&self, lower: u32, range: u32) ->u8 {
        //println!("\tget_symbol(lower: {:08x}, range: {:08x})", lower, range);
        // TODO: Fix this when I fix the other multiplier code
        let max_range = *self.frequencies.last().unwrap();
        debug_assert!(range >= max_range);
        let multiplier = range / max_range;

        let mut low = 0;
        let mut high = 255;
        while low != high {
            let mid = (low + high) / 2;
            if self.frequencies[mid] * multiplier >= lower {
                high = mid;
            }
            else {
                low = mid + 1;
            }
        }

        return low as u8;

        /* I think the above binary search is faster than either of the below
        linear searches, however I'm not entirely sure since when profiling the
        time spent in the function went down significantly, but the time spent
        in the file (1 file crate at the time) went up significantly, so I'm worried
        that it has slow side affects.


        for i in 0..256 {
            if self.frequencies[i] * multiplier >= lower {
                return i as u8;
            }
        }

        /*
        for (i, &upper) in self.frequencies.iter().enumerate() {
            if upper * multiplier >= lower {
                 // println!("\treturned {}", i);

                return i as u8;
            }
        }
        */

        panic!("invalid symbol");
        */
    }
}