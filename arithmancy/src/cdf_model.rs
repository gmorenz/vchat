use std::intrinsics::{fadd_fast, fsub_fast, fmul_fast, fdiv_fast};

use super::{Interval, Model};

pub trait CdfModel {
    fn mean(&self) -> u8;
    fn cdf(&self, val: f64) -> f64;
    fn icdf(&self, val: f64) -> f64;
}

impl <M: CdfModel> Model<u8> for M {
    fn get_interval(&self, sym: u8, range: u32) -> Interval {
        unsafe {
            let rel_sym = (sym.wrapping_sub(self.mean())) as i8 as f64;

            let f_sym = fsub_fast(rel_sym, 0.5);
            let c_sym = fadd_fast(rel_sym, 0.5);

            // These are scaled [0, 1];
            let lower = self.cdf(f_sym);
            let upper = self.cdf(c_sym);

            // TODO: Handle the case lower * range <= upper * range + 1
            // QUESTION: Actually can we not and just let that be lossless compression by
            // accident, choosing the next highest non-empty value? Possibly troublesome
            // at the upper end of the range.
            debug_assert!(lower * range as f64 + 1.0 < upper * range as f64, "lower: {}, upper: {}, range: {}", lower, upper, range);
            Interval {
                lower: (fmul_fast(lower, range as f64)) as u32,
                upper: (fmul_fast(upper, range as f64)) as u32 - 1,
            }
        }
    }

    fn get_symbol(&self, value: u32, range: u32) -> u8 {
        let middle = unsafe{ fdiv_fast(value as f64, range as f64) };

        let f64_sym = self.icdf(middle);
        let rel_sym = f64_sym.round() as i64 as u8;// https://github.com/rust-lang/rust/issues/10184

        let mut suspect = rel_sym.wrapping_add(self.mean());

        let mut interval = self.get_interval(suspect, range);
        let mut correction = 0;

        let mut add_not_sub = true;
        while interval.lower > value || value > interval.upper {
            correction += if add_not_sub{1} else {0};
            suspect = suspect.wrapping_add(if add_not_sub {correction} else {!correction});
            add_not_sub = !add_not_sub;

            interval = self.get_interval(suspect, range);
        }

        debug_assert!(interval.lower <= value);
        debug_assert!(value <= interval.upper);

        suspect
    }
}

/// A convenience trait to implement CdfModel for a probabiltiy distribution
/// by truncating the tail ends of the distribution and renormalizing based on
/// the remaining area.
/// TODO: Should I cache m somehow? How?
pub trait TruncatedCdfModel {
    fn tmean(&self) -> u8;
    fn untruncated_cdf(&self, val: f64) -> f64;
    fn untruncated_icdf(&self, val: f64) -> f64;
}

impl <T: TruncatedCdfModel> CdfModel for T {
    fn mean(&self) -> u8 { self.tmean() }

    fn cdf(&self, val: f64) -> f64 {
        unsafe {
            let lower = self.untruncated_cdf(-128.5);
            let upper = self.untruncated_cdf(127.5);
            let m = fsub_fast(upper, lower);

            fmul_fast(fsub_fast( self.untruncated_cdf(val), lower), fdiv_fast(1.0, m))
        }
    }

    fn icdf(&self, val: f64) -> f64 {
        unsafe {
            let lower = self.untruncated_cdf(-128.5);
            let upper = self.untruncated_cdf(127.5);
            let m = fsub_fast(upper, lower);

            let x = fadd_fast(fmul_fast(m, val), lower);
            self.untruncated_icdf(x)
        }
    }
}