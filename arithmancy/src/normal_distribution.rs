/// WIP, DO NOT USE.

use std::intrinsics::{fadd_fast, fsub_fast, fmul_fast, fdiv_fast, powif64, expf64}; // frem_fast, powf64

// An approximation to the normal distribution... designed as I go

use super::cdf_model::TruncatedCdfModel;

// This portion assumes centered on 0, stddev 1

unsafe fn pdf(val: f64) -> f64 {
    fdiv_fast(
        expf64(-fdiv_fast(powif64(val, 2), 2.0)),
        2.5066282746310002
    )

    // Before optimization: E.powf(-(val.powi(2))/2.0) / (2.0 * PI).sqrt()
}

/// Approximates the cdf of the normal distribution with mean=0, variance=1.
unsafe fn cdf(val: f64) -> f64 {
    // We use 26.2.16 from http://people.math.sfu.ca/~cbm/aands/page_932.htm to
    // get an approximation to the cdf for |val|
    let x = val.abs();
    const P: f64 = 0.33267;
    const A1: f64 = 0.4361836;
    const A2: f64 = -0.1201676;
    const A3: f64 = 0.9372980;

    let z = pdf(x);
    // let t = 1. / (1. + P * x);
    let t = fdiv_fast(1.0, fadd_fast(1.0, fmul_fast(P, x)));

    /* Replaced by slightly micro-optimized code, this is the simpler version.
    let res = 1. - z * (A1 * t + A2 * t.powi(2) + A3 * t.powi(3));

    // If val is negative we that the actual cdf is 1 - cdf(abs(val))
    if val < 0. {
        1.0 - res
    }
    else {
        res
    }*/

    // This is the more optmized version, which was then translated into using intrinsics.
    // let res = z * t * (A1 + A2 * t + A3 * (t * t));
    let res = fmul_fast(z, fmul_fast(t,
        fadd_fast(A1, fadd_fast(fmul_fast(A2, t), fmul_fast(A3, (powif64(t, 2)))))));

    if val < 0. {
        res
    }
    else {
        1.0 - res
    }
}

// polevl and p1evl translated from http://www.netlib.org/cephes/index.html, eval.tgz, polevl.c
// The functions there explicitly don't use checked arithmetic for C, we might consider doing the same.
// We do not use direct or even "correct" translations, though I believe they are correct on any sane use cases.
unsafe fn polevl( x: f64, mut coef: &[f64] ) -> f64 {
    let mut ans = coef[0];
    coef = &coef[1..];

    while coef.len() != 0 {
        ans = ans * x + coef[0];
        // For some reason the following optimized version is
        // *much* slower (increases run time from 52 secs to 220)
        // ans = fmul_fast(ans, fadd_fast(x, coef[0]));
        coef = &coef[1..];
    }

    ans
}

unsafe fn p1evl( x: f64, mut coef: &[f64] ) -> f64 {
    let mut ans = x + coef[0];
    coef = &coef[1..];

    while coef.len() != 0 {
        ans = ans * x + coef[0];
        // For some reason the following optimized version is
        // *much* slower (increases run time from 52 secs to 220)
        // ans = fmul_fast(ans, fadd_fast(x, coef[0]));
        coef = &coef[1..];
    }

    ans
}

unsafe fn icdf(mut y: f64) -> f64 {
    // Translation from http://www.netlib.org/cephes/index.html, cprob.tgz, ndtri
    const EXP_NEG_2: f64 = 0.13533528323661269189;
    #[allow(non_upper_case_globals)]
    const s2pi: f64 = 2.5066282746310002; // sqrt(2 * pi)
    static P0: [f64; 5] = [ // TODO: Why does the code use N=4 with this variable???
        -5.99633501014107895267E1,
        9.80010754185999661536E1,
        -5.66762857469070293439E1,
        1.39312609387279679503E1,
        -1.23916583867381258016E0
    ];
    static Q0: [f64; 8] = [
        /* 1.00000000000000000000E0,*/
        1.95448858338141759834E0,
        4.67627912898881538453E0,
        8.63602421390890590575E1,
        -2.25462687854119370527E2,
        2.00260212380060660359E2,
        -8.20372256168333339912E1,
        1.59056225126211695515E1,
        -1.18331621121330003142E0
    ];
    static P1: [f64; 9] = [
        4.05544892305962419923E0,
        3.15251094599893866154E1,
        5.71628192246421288162E1,
        4.40805073893200834700E1,
        1.46849561928858024014E1,
        2.18663306850790267539E0,
        -1.40256079171354495875E-1,
        -3.50424626827848203418E-2,
        -8.57456785154685413611E-4
    ];
    static Q1: [f64; 8] = [
        /*  1.00000000000000000000E0,*/
        1.57799883256466749731E1,
        4.53907635128879210584E1,
        4.13172038254672030440E1,
        1.50425385692907503408E1,
        2.50464946208309415979E0,
        -1.42182922854787788574E-1,
        -3.80806407691578277194E-2,
        -9.33259480895457427372E-4
    ];
    static P2: [f64; 9] = [
        3.23774891776946035970E0,
        6.91522889068984211695E0,
        3.93881025292474443415E0,
        1.33303460815807542389E0,
        2.01485389549179081538E-1,
        1.23716634817820021358E-2,
        3.01581553508235416007E-4,
        2.65806974686737550832E-6,
        6.23974539184983293730E-9
    ];
    static Q2: [f64; 8] = [
        /*  1.00000000000000000000E0,*/
        6.02427039364742014255E0,
        3.67983563856160859403E0,
        1.37702099489081330271E0,
        2.16236993594496635890E-1,
        1.34204006088543189037E-2,
        3.28014464682127739104E-4,
        2.89247864745380683936E-6,
        6.79019408009981274425E-9
    ];

    debug_assert!(y > 0.0);
    debug_assert!(y < 1.0);

    let mut code = 1;
    if y > fsub_fast(1.0, EXP_NEG_2) {
        y = fsub_fast(1.0, y);
        code = 0;
    }

    if y > EXP_NEG_2 {
        let y = fsub_fast(y, 0.5);
        let y2 = powif64(y, 2);
        let x = fadd_fast(y, fmul_fast(y, fmul_fast(y2, fdiv_fast(polevl( y2, &P0), p1evl( y2, &Q0)))));
        let x = fmul_fast(x, s2pi);
        return x;
    }

    let x = f64::sqrt( fmul_fast(-2.0, f64::ln(y)) );
    let x0 = fsub_fast(x, fdiv_fast(f64::ln( x ), x));

    let z = fdiv_fast(1.0, x);
    let x1 = if x < 8.0 {
        // P1, Q1?
        fmul_fast(z, fdiv_fast(polevl( z, &P1 ), p1evl( z, &Q1 )))
    }
    else {
        fmul_fast(z, fdiv_fast(polevl( z, &P2 ), p1evl( z, &Q2 )))
    };
    let mut x = fsub_fast(x0, x1);
    if code != 0 { x = -x };

    x
}

#[derive(Clone, Copy)]
pub struct TruncatedNormal {
    pub mean: u8,
    // Note that is variance is two low, far off numbers in the mean can cause the model to panic (debug) or cause corruption (release).
    // TODO: Calculate how small it can get before issues.
    pub variance: f64
}

impl TruncatedCdfModel for TruncatedNormal {
    fn tmean(&self) -> u8 { self.mean }

    fn untruncated_cdf(&self, val: f64) -> f64 {
        unsafe{ cdf(fdiv_fast(val, self.variance)) }
    }

    fn untruncated_icdf(&self, val: f64) -> f64 {
        unsafe { fmul_fast(icdf(val), self.variance) }
    }
}

#[cfg(test)]
mod primitive_test {
    use super::{cdf, icdf};
    #[test]
    fn cdficdf() {
        for i in 0.. 1000i32 {
            let x = (i - 500) as f64 / 100.;
            let y = unsafe{ icdf(cdf(x)) };
            let diff = (x - y).abs();
            // println!("{} : {}", x, y);
            debug_assert!(diff < 0.01);
        }

        for i in 0.. 1000i32 {
            let x = (i - 500) as f64 / 500.;
            let y = unsafe{ icdf(cdf(x)) };
            let diff = (x - y).abs();
            // println!("{} : {}", x, y);
            debug_assert!(diff < 0.0001);
        }
    }

    /* Todo: Test the TruncatedCdfModelTrait
    #[test]
    fn tcdfitcdf() {
        let min = 0. - 500. / 100.;
        let max = 999. - 500. / 100.;

        for i in 0.. 1000i32 {
            let x = (i - 500) as f64 / 100.;
            let y = unsafe{ itcdf(min, max, tcdf(min, max, x)) };
            let diff = (x - y).abs();
            // println!("{} : {}", x, y);
            debug_assert!(diff < 0.01);
        }

        let min = 0. - 500. / 500.;
        let max = 999. - 500. / 500.;
        for i in 0.. 1000i32 {
            let x = (i - 500) as f64 / 500.;
            let y = unsafe{ itcdf(min, max, tcdf(min, max, x)) };
            let diff = (x - y).abs();
            // println!("{} : {}", x, y);
            debug_assert!(diff < 0.0001);
        }
    }
    */
}

#[cfg(test)]
mod high_level_test {
    use super::TruncatedNormal;
    use super::super::{Decoder, Encoder};

    fn encrypt(data: &[u8]) -> Vec<bool> {
        let mut ret = vec!();
        let mut model = TruncatedNormal{ mean: 0, variance: 512. };
        let mut encoder = Encoder::new();
        for &x in data.iter() {
            encoder.encode(&mut model, |bit| ret.push(bit), x);
        }
        encoder.finalize(|bit| ret.push(bit));

        ret
    }

    fn decrypt(data: &[bool], out: &mut [u8]) {
        let mut model = TruncatedNormal{ mean: 0, variance: 512. };
        let mut decoder = Decoder::new(data.iter().map(|&x| x));
        for x in out.iter_mut() {
            *x = decoder.next(&mut model);
        }
    }

    #[test]
    fn simple() {
        const LEN: usize = 2;
        let data = vec![15; LEN];
        let mut out = vec![0; LEN];

        // println!("\n\nEncode\n");
        let compressed = encrypt(&data);
        // println!("\nDecode\n");
        decrypt(&compressed, &mut out);

        assert_eq!(data, out);
    }
}