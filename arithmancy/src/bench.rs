extern crate test;
extern crate rand;

use self::test::{black_box, Bencher};
use self::rand::Rng;

use super::{Encoder, AdaptiveFrequencyModel};

#[bench]
fn simple_encode(b: &mut Bencher) {
    let mut data = [0; 256];
    let mut rng = rand::IsaacRng::new_unseeded();
    for i in 0..256 {
        data[i] = rng.gen::<u8>();
    }
    b.iter(|| {
        let mut model = AdaptiveFrequencyModel::new();
        let mut encoder = Encoder::new();
        for &x in data.iter() {
            encoder.encode(&mut model, |bit| {black_box(bit);}, x);
            model.update(x);
        }
        encoder.finalize(|bit| {black_box(bit);});
    })
}